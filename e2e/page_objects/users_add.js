require('dotenv').config()

module.exports = {
  url: process.env.FRONTEND_URL + '/users/add',
  elements: {
    container: '.container',
    header: 'h1',
    inputFirstnme: '#firstname',
    inputLastname: '#lastname',
    inputUsername: '#username',
    inputPassword: '#password',
    inputPassword2: '#password2',
    buttonSubmit: 'button[type=submit]'
  },
  commands: [{
    checkMyHeader: function () {
      return this
        .waitForElementVisible('@header', 1000, function (result) {
          this.assert.equal(typeof result, 'object')
          this.assert.equal(result.status, 0)
        })
        .assert.containsText('@header', 'Pievienot lietotāju')
        .assert.urlEquals(this.url)
    },
    checkForWarning: function (field, warning) {
      return this
        .waitForElementVisible('span#' + field + '_msg', 1000)
        .assert.containsText('span#' + field + '_msg', warning)
    },
    checkForWarningNotVisible: function (field) {
      return this.expect.element('span#' + field + '_msg').to.not.be.visible
    },
    fillData: function (firstname, lastname, username, pass, pass2) {
      return this
        .waitForElementVisible('@inputFirstnme', 1000)
        .setValue('@inputFirstnme', firstname)
        .setValue('@inputLastname', lastname)
        .setValue('@inputUsername', username)
        .setValue('@inputPassword', pass)
        .setValue('@inputPassword2', pass2)
    },
    submit: function () {
      return this.click('@buttonSubmit', function (result) {
        this.assert.strictEqual(result.status, 0)
      })
    }
  }]
}
