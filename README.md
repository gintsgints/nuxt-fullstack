# NUXT fullstack
[![pipeline status](https://git.online.lv/ltt/node-js/nuxt-fullstack/badges/master/pipeline.svg)](https://git.online.lv/ltt/node-js/nuxt-fullstack/commits/master)

Frontend - standart NUXT generated app. https://nuxtjs.org/
Server made based on - https://github.com/Microsoft/TypeScript-Node-Starter
ORM - http://typeorm.io/#/

## Izstrāde

### Atbalsta dokera servisi
#### MySQL datubāzes docker

```
docker run --name nuxt-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root_234 -e MYSQL_DATABASE=nuxtdbdev -d mysql:5.7
```

### Izstrādes vides konfigurācija

Vides konfigurācijas parametri tiek nolasīti no faila .env 
Šajā failā jābūt aizpildītām vides mainīgo vērtībām, kas atbilst jūsu videi.
Failu var veidot arī kopējot no piemēra:

```
cp .example.env .env
```

un tad .env failā saraksta pareizos lietotāja datus.

### Lai darbinātu izstrādes serveri

``` bash
# install dependencies
$ yarn install

# go to API server module
$ cd packages/backend

# serve with hot reload at localhost:3001
$ yarn dev

```

Backend serveris darbojas uz adreses - http://localhost:3001/

### Lai frontend

``` bash
# install dependencies
$ yarn install

# go to API server module
$ cd packages/frontend

# serve with hot reload at localhost:3000
$ yarn dev

# generate static project
$ yarn generate
```

Frontend noklusēti tiek startēts uz - http://localhost:3000/

### Migrāciju izstrāde

Migrācijas var veidot ar globāli instalētu TypeORM tool:

``` bash
npm install -g typeorm
```

Pēc tam var ģenerēt migrācijas ar komandu:

``` bash
cd packages/backend
typeorm migration:create -n Nosaukums
```
migrācijas tiks ģenerētas direktorijā migrations. Tās var darbināt ar:

``` bash
yarn run migrate
```

Sīkāka info šeit - https://github.com/typeorm/typeorm/blob/master/docs/migrations.md

## UI Testi

Lai palaistu testus jāizpilda

``` bash
$ yarn e2e
```

Atseišķu pārlūku var izvēlēties (piem. Chrome):

``` bash
$ yarn e2e:chrome
```

Testus var atkļūdot ar Visual Studio Code.

Chrome testi tiek darbināti 'headless' režīmā. Ja nepieciešams redzēt pārlūku testa laikā, nightwatch.conf.js jāizņem opcija - 'headless'.

### UI testu darbināšana ar selenium serveri.

Testu darbināšanai automātiski tiek startēts Selenium serveris no npm pakotnes. Ja nepieciešams pārbaudīt selenium server
testu darbināšanu, tad jāatslēdz servera automātiska palaišana failā nightwatch.conf.js (start_process: false,) un jāpalaiž selenium serveris ar
komandu:

```
docker run -d --name selenium-hub -p 4444:4444 selenium/hub:3.10.0
docker run -d --name chrome -P -p 5900:5900 --link selenium-hub:hub --shm-size=2g selenium/node-chrome-debug:3.10.0
```

Tad jāuzstāda jūsu darba mašīna kā testa "hostname" (hostname jālieto mazie burti)

```
set FRONTEND_URL=http://dt00344
```

un var darbināt testu.

## Darbināšana uz PROD.

Lai sistēmu palaistu produkcijā, jāizmanto docker-compose. (failam docker-compose.yaml jābūt tekošajā direktorijā)

```bash
docker-compose up -d
```
