import errorHandler from 'errorhandler'
import { createConnection } from 'typeorm'
import http from 'http'
import express from 'express'
import { seed } from './seed'

interface MyExpress extends express.Express {
  server: http.Server
}

const app: MyExpress = require('./app')

/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler())

// create typeorm connection
createConnection().then(connection => {
  /**
   * Seed test data if developer environment
   */
  if (process.env.NODE_ENV === 'development') {
    seed()
  }

  /**
   * Start Express server.
   */
  app.server = app.listen(app.get('port'), () => {
    console.log(
      '  Aplikācija ir iestartēta http://localhost:%d %s režīmā',
      app.get('port'),
      app.get('env')
    )
    console.log('  Spiežat CTRL-C lai pārtrauktu\n')
    app.emit('appStarted')
    app.server.on('close', () => {
      connection.close()
    })
  })
})

export default app.server
