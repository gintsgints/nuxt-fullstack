import { MigrationInterface, QueryRunner } from 'typeorm'

export class InitUsers1530542855526 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`INSERT INTO user(id, firstName, lastName, username, password, role)
        VALUES (DEFAULT, 'Test', 'User', 'username', '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG', 'admin')`)
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
