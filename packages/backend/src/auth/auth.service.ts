import { Request, Response } from 'express'

import { getRepository } from 'typeorm'
import { User } from '../entities/user/user.model'

export function roleAuth (roles: Array<String>) {
    return async function(req: Request, res: Response, next: Function) {

        const user = req.user
        const foundUser = await getRepository(User).findByIds([user.id])

        if (foundUser.length === 0) {
            res.status(422).json({error: 'No user found.'})
            return next('No user found')
        }

        if (roles.indexOf(foundUser[0].role) > -1) {
            return next()
        }

        res.status(401).json({error: 'You are not authorized to view this content'})
        return next('Unauthorized')
    }
}