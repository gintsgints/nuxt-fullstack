const { client } = require('nightwatch-cucumber')
const { Given, Then } = require('cucumber')

Given(/^I open users page$/, () => {
  return client.page.users()
    .navigate()
})

Then(/^I get redirected to login page$/, () => {
  return client.page.login().checkMyHeader()
})

Then(/^I Log in as$/, (table) => {
  return client.page.login()
    .fillData(
      table.rowsHash().username,
      table.rowsHash().password)
    .submit()
})

Then(/^I log out$/, () => {
  return client.page.menu()
    .pushBtnAtslegties()
})

Then(/^I am at users list page$/, () => {
  return client.page.users().checkMyHeader()
})

Then(/^the button Pievienot exists$/, () => {
  return client.page.users().checkBtnPievienot()
})

Then(/^I push Pievienot button in users page$/, () => {
  return client.page.users().pushBtnPievienot()
})

Then(/^add users page opens$/, () => {
  return client.page.users_add().checkMyHeader()
})

Then(/^I am still at add users page$/, () => {
  return client.page.users_add().checkMyHeader()
})

Then(/^Field "([^"]*)" contains message "([^"]*)"$/, (field, message) => {
  return client.page.users_add().checkForWarning(field, message)
})

Then(/^Field "([^"]*)" has no message$/, (field) => {
  return client.page.users_add().checkForWarningNotVisible(field)
})

Given(/^I enter user data$/, (table) => {
  return client.page.users_add()
    .fillData(
      table.rowsHash().Firstname,
      table.rowsHash().Lastname,
      table.rowsHash().Username,
      table.rowsHash().password1,
      table.rowsHash().password2)
    .submit()
})
