require('dotenv').config()

module.exports = {
  url: process.env.FRONTEND_URL + '/login/',
  elements: {
    container: '.container',
    header: 'h1',
    inputUsername: '#username',
    inputPassword: '#password',
    buttonPieslegties: {
      selector: '//button[text()="Pieslēgties"]',
      locateStrategy: 'xpath'
    }
  },
  commands: [{
    checkMyHeader: function () {
      return this
        .waitForElementVisible('@header', 1000, function (result) {
          this.assert.equal(typeof result, 'object')
          this.assert.equal(result.status, 0)
        })
        .assert.containsText('@header', 'Pieslēgties')
    },
    checkBtnPieslegties: function () {
      return this.waitForElementVisible('@buttonPieslegties', 1000)
    },
    pushBtnPieslegties: function () {
      return this.click('@buttonPieslegties')
    },
    fillData: function (username, password) {
      return this
        .waitForElementVisible('@inputUsername', 1000)
        .setValue('@inputUsername', username)
        .setValue('@inputPassword', password)
    },
    submit: function () {
      return this.click('@buttonPieslegties', function (result) {
        this.assert.strictEqual(result.status, 0)
      })
    }
  }]
}
