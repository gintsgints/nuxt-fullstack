import { Entity, Column, PrimaryGeneratedColumn, BeforeUpdate } from 'typeorm'
import bcrypt from 'bcrypt'

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    firstName: string

    @Column()
    lastName: string

    @Column()
    username: string

    @Column({select: false})
    password: string

    @Column({default: 'user'})
    role: string

    @BeforeUpdate()
    async preSave() {
        const saltRounds = 10
        const hash = await bcrypt.hash(this.password, saltRounds)
        this.password = hash
    }

}
