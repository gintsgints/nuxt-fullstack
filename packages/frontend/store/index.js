import Vuex from 'vuex'
const cookieparser = require('cookieparser')

const createStore = () => {
  return new Vuex.Store({
    state: {
      auth: null
    },
    mutations: {
      SET_AUTH (state, data) {
        this.$axios.setToken(data, 'Bearer')
        state.auth = data
      }
    },
    actions: {
      nuxtServerInit ({ commit }, { req }) {
        let accessToken = null
        if (req.headers.cookie) {
          accessToken = cookieparser.parse(req.headers.cookie)
        }
        if (accessToken) {
          commit('SET_AUTH', accessToken.auth)
        }
      }
    }
  })
}

export default createStore
