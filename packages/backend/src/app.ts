import express from 'express'
import passport from 'passport'
import * as bodyParser from 'body-parser'

require('dotenv').config()

const app = express()
app.use(bodyParser.json())

const requireLogin = passport.authenticate('jwt', {session: false})

// register routes
app.use('/api/auth', require('./auth').default)
app.use('/api/users', requireLogin, require('./entities/user'))

// Express configuration
app.set('port', process.env.BACK_PORT)

export default app

module.exports = app