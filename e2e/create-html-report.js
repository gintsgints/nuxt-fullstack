var reporter = require('cucumber-html-reporter')

var options = {
  theme: 'bootstrap',
  jsonFile: './reports/cucumber.json',
  output: './reports/cucumber.html',
  reportSuiteAsScenarios: true
}

reporter.generate(options)
