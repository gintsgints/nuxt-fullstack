'use strict'

import express from 'express'
import passport from 'passport'
import jwt from 'jsonwebtoken'
import { Request, Response } from 'express'

const requireLogin = passport.authenticate('local', {session: false })

const router = express.Router()

function setUserInfo(request: any) {
  return {
      id: request.id,
      email: request.email,
      role: request.role
  }
}

function generateToken(user: any) {
  return jwt.sign(user, process.env.SECRET, {
      expiresIn: 10080
  })
}

router.post('/', requireLogin, async function(req: Request, res: Response, next) {
  const userInfo = setUserInfo(req.user)

  res.status(200).json({
      token: generateToken(userInfo),
      user: userInfo
  })
})

export default router
