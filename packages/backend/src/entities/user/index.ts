import express from 'express'

import { roleAuth } from '../../auth/auth.service'

import * as controller from './user.controller'

const router = express.Router()

router.get('/', roleAuth(['admin']), controller.index)
router.get('/:id', roleAuth(['admin']), controller.show)
router.post('/', roleAuth(['admin']), controller.create)
router.put('/:id', roleAuth(['admin']), controller.upsert)
router.delete('/:id', roleAuth(['admin']), controller.destroy)

module.exports = router
