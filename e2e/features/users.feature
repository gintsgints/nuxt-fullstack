Feature: Admin create user

Scenario: When password is different and Firstname contains letters and password does not fit rulles admin get apropriate warning

  Given I open users page
  Then I get redirected to login page
  Then I Log in as
    | username | username |
    | password | passwsss |
  Then I am at users list page
  Then the button Pievienot exists
  And I push Pievienot button in users page
  Then add users page opens
  Given I enter user data
    | Firstname | 234Aklj |
    | Lastname  | Lastname  |
    | Username  | Username  |
    | password1 | password1 |
    | password2 | password2 |
  Then I am still at add users page
  And Field "firstname" contains message "var ievadīt tikai burtus"
  And Field "password" contains message "Parolei jāsatur vismaz vienu lielo burtu"
  And Field "password2" contains message "vērtība nav korekta"
  Then I log out

Scenario: When there is no username admin get warning

  Given I open users page
  Then I get redirected to login page
  Then I Log in as
    | username | username |
    | password | passwsss |
  Then I am at users list page
  Then the button Pievienot exists
  And I push Pievienot button in users page
  Then add users page opens
  Given I enter user data
    | Firstname | Firstname |
    | Lastname  | Lastname  |
    | Username  |  |
    | password1 | Letter_23 |
    | password2 | Letter_23 |
  Then I am still at add users page
  And Field "username" contains message "ir obligāti aizpildāms"
  And Field "password2" has no message
  Then I log out

Scenario: When all fields are correct Admin is able to register user

  Given I open users page
  Then I get redirected to login page
  Then I Log in as
    | username | username |
    | password | passwsss |
  Then I am at users list page
  Then the button Pievienot exists
  And I push Pievienot button in users page
  Then add users page opens
  Given I enter user data
    | Firstname | Firstname |
    | Lastname  | Lastname  |
    | Username  | flastname |
    | password1 | Letter_23 |
    | password2 | Letter_23 |
  Then I am at users list page
  Then I log out
