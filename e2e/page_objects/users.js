require('dotenv').config()

module.exports = {
  url: process.env.FRONTEND_URL + '/users/',
  elements: {
    container: '.container',
    header: 'h1',
    buttonPievienot: {
      selector: '//button[text()="Pievienot"]',
      locateStrategy: 'xpath'
    }
  },
  commands: [{
    checkMyHeader: function () {
      return this
        .waitForElementVisible('@header', 1000, function (result) {
          this.assert.equal(typeof result, 'object')
          this.assert.equal(result.status, 0)
        })
        .waitForElementVisible('table', 1000, function (result) {
          this.assert.equal(typeof result, 'object')
          this.assert.equal(result.status, 0)
        })
        .assert.containsText('@header', 'Lietotāji:')
        .assert.urlEquals(this.url)
    },
    checkBtnPievienot: function () {
      return this.waitForElementVisible('@buttonPievienot', 1000)
    },
    pushBtnPievienot: function () {
      return this.click('@buttonPievienot')
    }
  }]
}
