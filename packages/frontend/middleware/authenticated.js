import Cookie from 'js-cookie'

export default function ({ store, route, redirect }) {
  // If the user is not authenticated
  if (!store.state.auth) {
    // Try get from token
    const auth = Cookie.get('auth')
    if (auth) {
      store.commit('SET_AUTH', auth)
    } else {
      return redirect('/login?next=' + route.fullPath)
    }
  } else {
    store.$axios.setToken(store.state.auth, 'Bearer')
  }
}
