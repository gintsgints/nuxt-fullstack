'use strict'
import express from 'express'

// Passport Configuration
require('./local/passport').setup()

const router = express.Router()

router.use('/local', require('./local').default)

export default router
