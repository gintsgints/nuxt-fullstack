import request from 'supertest'
import { expect } from 'chai'
import { createQueryBuilder, getRepository } from 'typeorm'
import { User } from './user.model'

const app = require('../../app')
const server = require('../../server')

let newUser: User

describe('User API', () => {
  let admin_token = ''
  let admin: User = undefined
  before((done) => {
    app.on('appStarted', async () => {
      /* Admin user for tests */
      const user1 = new User()
      user1.firstName = 'Admin'
      user1.lastName = 'Admin'
      user1.username = 'admin'
      user1.password = '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG'
      user1.role = 'admin'
      admin = await getRepository(User).save(user1)
      request(app)
        .post('/api/auth/local')
        .send({username: admin.username, password: 'passwsss'})
        .expect(200)
        .end((err, res: any) => {
          if (err) {
            done(err)
          }
          admin_token = `Bearer ${res.body.token}`
          done()
        })
    })
  })

  after((done) => {
    const delAll = async () => {
      const qry = createQueryBuilder()
      await qry.delete()
        .from(User)
        .where({username: admin.username})
        .execute()
      return app.server.close(done)
    }
    delAll()
  })

  describe('GET /api/users - unauthorized', () => {
    beforeEach((done) => {
      request(app)
        .get('/api/users')
        .expect(401)
        .end((err, res) => {
          if (err) {
            done(err)
          }
          done()
        })
    })
  })

  describe('GET /api/users', () => {
    let users: User[]

    beforeEach((done) => {
      request(app)
        .get('/api/users')
        .set('Authorization', admin_token)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            done(err)
          }
          users = res.body
          done()
        })
    })
    it('ir jāatgriež JSON array', () => {
        expect(users).to.be.instanceOf(Array)
    })
  })

  describe('POST /api/users', () => {
      beforeEach((done) => {
        request(app)
          .post('/api/users')
          .set('Authorization', admin_token)
          .send({
            firstName: 'First',
            lastName: 'Last',
            username: 'username',
            password: 'password',
            role: 'user'
          })
          .expect(201)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            if (err) {
              return done(err)
            }
            newUser = res.body
            done()
          })
      })

      it('jāatbild atsūtot jaunizveidoto lietotāju', function() {
        expect(newUser.firstName).to.equal('First')
        expect(newUser.lastName).to.equal('Last')
        expect(newUser).to.not.have.property('password')
      })
  })

  describe('GET /api/users/:id', function() {
      let user: User

      beforeEach(function(done) {
        request(app)
          .get(`/api/users/${newUser.id}`)
          .set('Authorization', admin_token)
          .expect(200)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            if (err) {
              return done(err)
            }
            user = res.body
            done()
          })
      })

      afterEach(function() {
        user = new User()
      })

      it('ir jāatgriež pieprasītie lietotāja dati', function() {
        expect(user.firstName).to.equal('First')
        expect(user.lastName).to.equal('Last')
        expect(user).to.not.have.property('password')
      })
    })

  describe('PUT /api/users/:id', function() {
      let rawData: any

      beforeEach(function(done) {
        request(app)
          .put(`/api/users/${newUser.id}`)
          .set('Authorization', admin_token)
          .send({
            firstName: 'Firstupd',
            lastName: 'Lastupd'
          })
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function(err, res) {
            if (err) {
              return done(err)
            }
            rawData = res.body
            done()
          })
      })

      it('jāatgriež info par vienu labotu ierakstu', function() {
        expect(rawData.affectedRows).to.exist
        expect(rawData.affectedRows).to.equal(1)
      })

      it('pēc datu atjaunošanas GET ir jāatgriež labotie dati', function(done) {
        request(app)
          .get(`/api/users/${newUser.id}`)
          .set('Authorization', admin_token)
          .expect(200)
          .expect('Content-Type', /json/)
          .end((err, res) => {
            if (err) {
              return done(err)
            }
            const user = res.body

            expect(user.firstName).to.equal('Firstupd')
            expect(user.lastName).to.equal('Lastupd')

            done()
          })
      })
    })

  describe('DELETE /api/users/:id', function() {
      it('jāatgriež atbilde ar kodu 204, kas liecina par veiksmīgu dzēšanu', function(done) {
        request(app)
          .delete(`/api/users/${newUser.id}`)
          .set('Authorization', admin_token)
          .expect(204)
          .end(err => {
            if (err) {
              return done(err)
            }
            done()
          })
      })

      it('jāatgriež 404, ja lietotājs neeksistē', function(done) {
        request(app)
          .delete(`/api/users/${newUser.id}`)
          .set('Authorization', admin_token)
          .expect(404)
          .end(err => {
            if (err) {
              return done(err)
            }
            done()
          })
      })
  })
})
