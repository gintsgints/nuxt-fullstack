import { createQueryBuilder, getRepository } from 'typeorm'
import { User } from './entities/user/user.model'

export async function seed() {
    console.log('Filling database with test data...')
    const qry = createQueryBuilder()

    try {
        /* Cleanup */
        await qry.delete()
        .from(User)
        .execute()

        /* Users */
        const user1 = new User()
        user1.firstName = 'Test'
        user1.lastName = 'User'
        user1.username = 'username'
        // passwsss
        user1.password = '$2b$10$IqwMpSua6GLwH8KXWf71UeUR7GlfDyMp0gJxljKOYmW4jGksmwDVG'
        user1.role = 'admin'
        await getRepository(User).save(user1)
    } catch (error) {
        console.log('Internal error while seed.', error)
    }

}