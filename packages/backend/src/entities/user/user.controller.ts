import { Request, Response } from 'express'
import { getRepository } from 'typeorm'
import { User } from './user.model'

// Gets list of users
export async function index(req: Request, res: Response) {
    // const users = testing()
    const users = await getRepository(User).find()
    res.send(users)
}

export async function show(req: Request, res: Response) {
    const user = await getRepository(User).findOne(req.params.id)
    res.send(user)
}

export async function create(req: Request, res: Response) {
    const user = getRepository(User).create(req.body)
    // For some reason getRepository states it return array, but it's not. So I specify any.
    const saved: any = await getRepository(User).save(user)
    // save still returns password, even it is said to not in model
    delete saved.password
    res.status(201).send(saved)
}

export async function upsert(req: Request, res: Response) {
    try {
        const user: any = await getRepository(User).update(req.params.id, req.body)
        res.send(user.raw)
    } catch (error) {
        res.status(404).send(error)
    }
}

export async function destroy(req: Request, res: Response) {
    const user = await getRepository(User).findOne(req.params.id)
    if (user) {
        getRepository(User).remove(user)
        res.sendStatus(204)
    } else {
        res.sendStatus(404)
    }
}
