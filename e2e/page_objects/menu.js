require('dotenv').config()

module.exports = {
  elements: {
    buttonAtslegties: {
      selector: '//a[text()="Atslēgties"]',
      locateStrategy: 'xpath'
    }
  },
  commands: [{
    pushBtnAtslegties: function () {
      return this.click('@buttonAtslegties')
    }
  }]
}
