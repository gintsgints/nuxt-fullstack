import passport from 'passport'
import bcrypt from 'bcrypt'

import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt'
import { Strategy as LocalStrategy } from 'passport-local'

import { getRepository } from 'typeorm'
import { User } from '../../entities/user/user.model'

export function setup() {
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
    secretOrKey: process.env.SECRET
  }

  passport.use(new JwtStrategy(jwtOptions, async function(jwt_payload, done: Function) {
    const user = await getRepository(User)
      .createQueryBuilder()
      .addSelect('id, password')
      .where('id = :id', { id: jwt_payload.id })
      .getRawOne()
    if (user === undefined) {
      return done(undefined, false, { message: 'This user is not registered' })
    }
    return done(undefined, user, { message: 'success'})
  }))

  passport.use(new LocalStrategy(async function(username: string, password: string, done: Function) {
    const user = await getRepository(User)
      .createQueryBuilder()
      .addSelect('id, password')
      .where('username = :username', { username: username })
      .getRawOne()
    if (!user) {
      return done(undefined, false, { message: 'This user is not registered' })
    }
    const matches = await bcrypt.compare(password, user.password)
    if (matches) {
      return done(undefined, user, { message: 'success'})
    } else {
      return done(undefined, false, { message: 'Password is not correct' })
    }
  }))
}
